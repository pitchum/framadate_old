This is a fork of framadate (which itself is a fork of STUdS).
While STUdS is unmaintained, the framadate project is being actively developed
and its official repository is hosted on
[git.framasoft.org](https://git.framasoft.org/framasoft/framadate).

Original STUdS project is hosted on [sourcesup.cru.fr](https://sourcesup.cru.fr/projects/studs/)

Main authors of Framadate are:

- Simon LEBLANC
- Pierre-Yves GOSSET

Main authors of STUdS are:

- Guilhem BORGHESI
- Raphaël DROZ


## Installation

These instructions are valid for an installation on Debian GNU/Linux.
You may need to adapt some of these for your distribution.

Install required dependencies.

    # apt-get install php-gettext mysql-server php5-adodb ngninx-light php5-fpm

Create a dedicated user named framadate.

    useradd -U framadate
    mkdir /srv/framadate
    chown framadate: /srv/framadate

    su - framadate
    cd /srv/framadate
    git clone https://git.framasoft.org/framasoft/framadate.git .
    # or git clone https://git.framasoft.org/pitchum/framadate.git .

You need to use composer to install some specific dependencies.

    php -r "readfile('https://getcomposer.org/installer');" | php
    ./composer.phar install

Prepare the database.

    mysql -uroot -p
    > CREATE USER 'framadate'@'localhost' IDENTIFIED BY 'S3cr3tP4ssw0rd';
    > CREATE DATABASE framadate;
    > GRANT ALL on framadate.* to 'framadate'@'localhost';
    > exit
    mysql -uframadate -pS3cr3tP4ssw0rd < install/install.mysql.sql

Copy file app/inc/constants.php.template as app/inc/constants.php and edit it.

    const BASE = 'framadate';
    const USERBASE = "framadate";
    const USERPASSWD = 'S3cr3tP4ssw0rd';
    const SERVEURBASE = 'localhost';
    const BASE_TYPE = 'mysql';

## Nginx config

Here we'll describe two distinct fashions of deploying framdate.
You have to choose between a dedicated virtualhost or a simple webapp.
The difference is in the URL of your app:

- virtualhost URL: http://framadate.example.net/
- simple webapp URL: http://example.net/framadate/

### Nginx config with a dedicated virtualhost

This config requires a dedicated DNS hostname.

*TODO*

### Nginx config for a simple webapp

With this config, Framadate will be availaible at:
http://www.example.net/framadate/

    server {
        ...
        server_name www.example.net;
        ...

        location /framadate/ {
            alias /srv/framadate/;
            index index.php;
            # "Clean" URLs
            rewrite "^/framadate/([a-zA-Z0-9]+)$" "/framadate/studs.php?poll=$1" last;
            rewrite "^/framadate/([a-zA-Z0-9]+)/action/([a-zA-Z_-]+)/(.+)$" "/framadate/studs.php?poll=$1&$2=$3" last;
            rewrite "^/framadate/([a-zA-Z0-9]+)/vote/([a-zA-Z0-9]{16})$" "/framadate/studs.php?poll=$1&vote=$2" last;
            rewrite "^/framadate/([a-zA-Z0-9]{24})/admin$" "/framadate/adminstuds.php?poll=$1" last;
            rewrite "^/framadate/([a-zA-Z0-9]{24})/admin/vote/([a-zA-Z0-9]{16})$" "/framadate/adminstuds.php?poll=$1&vote=$2" last;
            rewrite "^/framadate/([a-zA-Z0-9]{24})/admin/action/([a-zA-Z_-]+)(/(.+))?$" "/framadate/adminstuds.php?poll=$1&$2=$3" last;
            
            location ~ \.php$ {
                fastcgi_index index.php;
                include /etc/nginx/fastcgi_params;
                fastcgi_pass unix:/run/php5-fpm.sock;
                fastcgi_param SERVER_NAME www.example.net;
            }
        }

        ...

    }

Also edit /srv/framadate/app/inc/constants.php like this:

    const URL_PROPRE = true;


## LICENSE

==========================================================================

Université de Strasbourg - Direction Informatique
Auteur : Guilhem BORGHESI
Création : Février 2008

borghesi@unistra.fr

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site "http://www.cecill.info".

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.
Vous pouvez trouver une copie de la licence dans le fichier LICENSE.

==========================================================================

Université de Strasbourg - Direction Informatique
Author : Guilhem BORGHESI
Creation : Feb 2008

borghesi@unistra.fr

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms. You can
find a copy of this license in the file LICENSE.

==========================================================================
